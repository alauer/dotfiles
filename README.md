# dotfiles

## For WSL Ubuntu

WSL Ubuntu does not have Python installed by default.
This will pull into all the necessary dependencies to install ansible.

```bash
sudo apt intall ansible
```

## Ansible to deploy everything (preferred)

```bash
git clone --recurse-submodules https://gitlab.com/alauer/dotfiles.git ~/.dotfiles
ansible-galaxy collection install -r ~/.dotfiles/ansible/requirements.yml
ansible-playbook ~/.dotfiles/ansible/playbook.yml -c local --ask-become-pass
```

## Manual Install

```bash
git clone --recurse-submodules https://gitlab.com/alauer/dotfiles.git ~/.dotfiles; ~/.dotfiles/install.sh
```

## Upgrade

```bash
~/.dotfiles/upgrade.sh
```
