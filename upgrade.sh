#!/usr/bin/env bash
# shellcheck disable=SC2034,SC1090
dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
pushd "${dir}" || exit

git pull

# OS Specific packages
cp -rfs "${dir}"/config/. "$HOME"/
if [ "$(uname)" == "Darwin" ]; then
    cp -rs "${dir}"/config.osx/. "$HOME"/
elif grep -q microsoft /proc/version; then
        cp -rs "${dir}"/config.ubuntu/. "$HOME"/
elif grep -q Linux /proc/version; then
        cp -rs "${dir}"/config.ubuntu/. "$HOME"/
fi

# Run the upgrade all script for this system
upgrade-all

# Upgrade oh-my-zsh
upgrade_oh_my_zsh

popd || exit
