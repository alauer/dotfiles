# Debug start up times
# zmodload zsh/zprof

# HomeBrew Cask Options
export HOMEBREW_CASK_OPTS="--appdir=/Applications"

# Docker
# eval $(docker-machine env default)

# TheFuck
# eval "$(thefuck --alias)"

# Export brew packages
export PATH=$PATH:/usr/local/sbin