#!/usr/bin/env bash
# shellcheck disable=SC2034,SC1090
sudo apt-get install -y -qq python3-pip jq

sudo pip3 install apt-smart
sudo apt-smart --auto-change-mirror
sudo apt-get update -qq

# Neovim PPA
sudo add-apt-repository ppa:neovim-ppa/unstable
sudo apt-get update -qq

# Base
sudo apt-get install -y -qq make build-essential cmake gfortran \
    libatlas-base-dev python-dev python3-dev \
    build-essential libssl-dev zlib1g-dev libbz2-dev \
    libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev \
    libncursesw5-dev xz-utils tk-dev libffi-dev liblzma-dev \
    python-openssl git python3-pip jq vim neovim unzip zsh

# Install Headers
#sudo apt-get --yes install linux-image-extra-virtual

# Oh-my-Zsh and plugins
# chsh -s `which zsh`
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended --keep-zshrc
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git "${ZSH_CUSTOM:-~/.oh-my-zsh/custom}"/plugins/zsh-syntax-highlighting

# Powerlevel 10000 theme
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git "${ZSH_CUSTOM:-~/.oh-my-zsh/custom}"/themes/powerlevel10k

# Powerline fonts
mkdir -p "${HOME}"/.fonts
git clone https://github.com/powerline/fonts "${HOME}"/.fonts/powerline-fonts/
source "${HOME}"/.fonts/powerline-fonts/install.sh

# Starship
sh -c "$(curl -fsSL https://starship.rs/install.sh)"

#AWS CLI
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "/tmp/awscliv2.zip"
unzip -qq /tmp/awscliv2.zip -d /tmp/
/tmp/aws/install -i "$HOME"/.aws-cli -b "$HOME"/bin