#!/usr/bin/env bash
# shellcheck disable=SC2034,SC1090
# Cli Tools
xcode-select --install

# HomeBrew and Casks
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
#brew tap homebrew/science
#brew tap neovim/neovim
brew tap caskroom/cask
brew tap caskroom/versions
brew tap caskroom/fonts
export HOMEBREW_CASK_OPTS="--appdir=/Applications"


# Fonts
brew cask install font-hack-nerd-font

# A good set of terminal tools
brew install zsh
chsh -s "$(which zsh)"
brew install copy-ssh-id
#brew install subversion
brew install git
brew install git-extras
#brew install tig
#brew install hub
brew install macvim
brew install neovim
#brew cask install java
#brew install maven
#brew install gradle
brew install nvm
brew install pyenv
brew install rbenv
brew install ruby-build
brew install mtr
brew install wget
brew install fontconfig
brew install fortune
brew install cowsay
brew install ansible


# Nice apps for Dev
brew cask install sublime-text3
brew cask install iterm2
brew cask install google-chrome
brew cask install firefox
brew cask install atom
brew install go
brew install azure-cli
brew install media-info
brew install ffmpeg --with-fdk-aac --with-libass --with-x265 --with-openjpeg --with-fontconfig --with-freetype --with-libvpx
brew install terraform
brew cask install google-cloud-sdk
brew install starship

# Docker
#brew cask install virtualbox
#brew install docker
#brew install docker-machine
#brew install docker-swarm
#docker-machine create --driver virtualbox default
#brew services start docker-machine # starts docker default machine up on restart

# For fun R/Hive/Pig/Spark
#brew install gcc
#brew install r
#brew install hive
#brew install pig
#brew install apache-spark

# Oh My ZSH
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)" --unattended --keep-zshrc
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git "${ZSH_CUSTOM:-~/.oh-my-zsh/custom}"/plugins/zsh-syntax-highlighting

# Powerlevel 10000 theme
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git "${ZSH_CUSTOM:-~/.oh-my-zsh/custom}"/themes/powerlevel10k

# Latest Node
latest="$(nvm ls-remote | grep -E '^[[:space:]]+v[[:digit:]]+\.[[:digit:]]+\.[[:digit:]]+$' | tail -1 | xargs echo)"
nvm install "${latest}"
nvm alias default "${latest}"
packages="$(tr '\n' ' ' < js/global-npm.txt)"
# Configure npm to install global packages in home Directory
mkdir ~/.npm-global
npm config set prefix "$(~/.npm-global)"
export PATH=~/.npm-global/bin:$PATH
source ~/.profile
# shellcheck disable=SC2016
echo 'export PATH=~/.npm-global/bin:$PATH' >> ~/.profile
source ~/.profile
npm install -g "${packages}"

# Configure Atom to restore Gist backup
apm install sync-settings
