#!/usr/bin/env bash
# shellcheck disable=SC2034,SC1090
dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# OS Specific packages
cp -rfs "${dir}"/config/. "$HOME"/
if [ "$(uname)" == "Darwin" ]; then
    cp -rs "${dir}"/config.osx/. "$HOME"/
    mkdir "$HOME"/bin
    source "${dir}"/install.osx.sh
elif grep -q microsoft /proc/version; then
        cp -rs "${dir}"/config.ubuntu/. "$HOME"/
        mkdir "$HOME"/bin
        source "${dir}"/install.wsl.sh
elif grep -q Linux /proc/version; then
        cp -rs "${dir}"/config.ubuntu/. "$HOME"/
        mkdir "$HOME"/bin
        source "${dir}"/install.ubuntu.sh
fi

# Pyenv
if [ ! -d "$HOME/.pyenv" ]; then
    git clone https://github.com/yyuu/pyenv.git "$HOME"/.pyenv
    git clone https://github.com/pyenv/pyenv-virtualenv.git "$HOME"/.pyenv/plugins/pyenv-virtualenv
    git clone https://github.com/pyenv/pyenv-which-ext.git "$HOME"/.pyenv/plugins/pyenv-which-ext

fi

if [ -f "$HOME/.pyenvrc" ]; then
    "${dir}"/install_python.zsh python/requirements.txt
fi