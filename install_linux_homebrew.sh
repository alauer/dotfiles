#!/usr/bin/env bash

sudo apt-get install -y build-essential libssl-dev zlib1g-dev libbz2-dev \
libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev \
xz-utils tk-dev libffi-dev liblzma-dev python-openssl git
sudo apt-get install python3-pip jq

sudo pip3 install apt-select
apt-select -C "$(curl -s ipinfo.io/ | jq -r ".country")"
sudo mv sources.list /etc/apt/

# Installs Homebrew for Linux and builds environment
sudo apt-get install build-essential curl file git wget vim unzip

/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"

test -d ~/.linuxbrew && eval "$(~/.linuxbrew/bin/brew shellenv)"
test -d /home/linuxbrew/.linuxbrew && eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
test -r ~/.bash_profile && echo "eval \$($(brew --prefix)/bin/brew shellenv)" >>~/.bash_profile
echo "eval \$($(brew --prefix)/bin/brew shellenv)" >>~/.zprofile

# Oh-my-Zsh
sudo apt-get --yes install zsh
chsh -s "$(which zsh)"
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended --keep-zshrc

# Powerlevel 10000 theme
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git "${ZSH_CUSTOM:-~/.oh-my-zsh/custom}"/themes/powerlevel10k

# Install Powershell
wget -q -O "${HOME}"/packages-microsoft-prod.deb https://packages.microsoft.com/config/ubuntu/18.04/packages-microsoft-prod.deb
sudo dpkg -i "${HOME}"/packages-microsoft-prod.deb
sudo apt-get update
sudo add-apt-repository universe
sudo apt-get install -y powershell
rm -f "${HOME}"/packages-microsoft-prod.deb*

# Powerline fonts
#mkdir -p ${HOME}/.fonts
#git clone https://github.com/powerline/fonts ${HOME}/.fonts/powerline-fonts/
#${HOME}/.fonts/powerline-fonts/install.sh


# Install AWS CLI v2 - two methods here
#curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "~/awscliv2.zip"
#unzip ~/awscliv2.zip
#sudo ~/aws/install

brew install awscli
brew install starship

