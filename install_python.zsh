#!/usr/bin/env zsh
args=("$@")
print ${1}

[ -e "${HOME}/.zshenv" ] && source "${HOME}/.zshenv"
[ -e "${HOME}/.zprofile" ] && source "${HOME}/.zprofile"
[ -e "${HOME}/.zshrc" ] && source "${HOME}/.zshrc"
[ -e "${HOME}/.pyenvrc" ] && source "${HOME}/.pyenvrc"

# Latest Python
latest=`pyenv install -l | egrep '^[[:space:]]+[[:digit:]]+\.[[:digit:]]+\.[[:digit:]]+$' | tail -1 | xargs echo`
pyenv install ${latest}
pyenv global ${latest}
pyenv shell ${latest}
pip install -r ${1}
